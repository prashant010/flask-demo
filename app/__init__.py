import logging

from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

import lib.log as log
from config import config, APP_NAME

Logger = logging.getLogger(APP_NAME)
db = SQLAlchemy()
ma = Marshmallow()


def initialize_db(app):
    db.init_app(app)
    ma.init_app(app)
    migrate = Migrate(app, db)


def create_app(config_name):
    app = Flask(__name__)
    
    CORS(app,resources={r"/*":{"origins":"*"}})
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    log.setup_logging(config[config_name])

    initialize_db(app)

    # from app.authorization.views_auth import auth_bp as auth_router
    # app.register_blueprint(auth_router, url_prefix='/auth')

    return app
