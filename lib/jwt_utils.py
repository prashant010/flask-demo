from datetime import datetime, timedelta
from functools import wraps

import jwt
from flask import current_app, jsonify
from flask import g
from flask import request


def create_token(user):
    payload = {
        'sub': user.id,
        'name': user.display_name,
        'username':user.username,
        'email': user.email,
        'mobile': user.mobile,
        'role':user.role,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=15)
    }
    token = jwt.encode(payload, current_app.config['TOKEN_SECRET'])
    return token.decode('unicode_escape')

def create_temp_token(user):
    payload = {
        'sub': user.id,
        'name': user.display_name,
        'username': user.username,
        'email': user.email,
        'mobile': user.mobile,
        'role': user.role,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=1)
    }
    token = jwt.encode(payload, current_app.config['TOKEN_SECRET'])
    return token.decode('unicode_escape')


def parse_token(req):

    token = req.headers.get('Authorization').split()[0]

    return jwt.decode(token, current_app.config['TOKEN_SECRET'])


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            payload = parse_token(request)

        except jwt.DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except jwt.ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response
        
        g.user_id = payload['sub']
        g.user_name = payload['name']
        g.user_username = payload['username']
        g.user_mobile = payload['mobile']
        g.user_email = payload['email']
        if 'role' in payload:
            g.role = payload['role']

        return f(*args, **kwargs)

    return decorated_function


def login_optional(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('Authorization'):
            
            try:
                payload = parse_token(request)

            except jwt.DecodeError:
                response = jsonify(message='Token is invalid')
                response.status_code = 401
                return response
            except jwt.ExpiredSignature:
                response = jsonify(message='Token has expired')
                response.status_code = 401
                return response
            
            g.user_id = payload['sub']
            g.user_name = payload['name']
            g.role = payload['role']
            g.user_mobile = payload['mobile']
            g.user_email = payload['email']
        return f(*args, **kwargs)

    return decorated_function

