from information import *

HOME = "flask-demo"
LOG_DIR = "/"
LOG_FILE = "info.log"
DEBUG_LOG_FILE = "debug.log"
ERROR_LOG_FILE = "error.log"
DB_FILE = "db.log"
PORT = 5001
APP_NAME = "flask-demo"


class Config:
    DEBUG = False
    TESTING = False
    API_TIMEOUT = 5

    def __init__(self):
        pass

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    HOME = "flask-demo"
    ENV = "development"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = (
        "mysql+pymysql://"
        + DB_USER
        + ":"
        + DB_PASSWORD
        + "@"
        + DB_INSTANCE
        + "/"
        + DB_DATABASE
    )
    SECRET_KEY = "flask-demo"
    SQLALCHEMY_TRACK_MODIFICATIONS = True


config = {"development": DevelopmentConfig, "default": DevelopmentConfig}
